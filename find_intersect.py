
"""
Найти пересечения в двух массивах
Например, даны два массива
[1, 2, 3, 2, 0]
[5, 1, 2, 7, 3, 2]
Надо вернуть [1, 2, 2, 3] (порядок неважен)
"""

find_intersect([], []) # []
find_intersect([1], []) # []
find_intersect([], [1]) # []
find_intersect([1,2], [1,0]) # [1]
find_intersect([1,2], [0,1]) # [1]
find_intersect([1,2], [1,2]) # [1,2]
find_intersect([1,2], [2,1]) # [1,2]
find_intersect([2,1], [1,2]) # [2,1]

find_intersect([2,1], [1,2]) # [2,1]


find_intersect([2,2,2], [2]) # [2]
find_intersect([2,2,2], [2,2]) # [2, 2]


def find_intersect(arr_1 , arr_2):
    result = []
    dict_1 = defaultdict(int)
    
    
    for item in arr_1:
        dict_1[item] += 1

    
    for item in arr_2:
        if item in dict_1 and dict_1[item] > 0:
            dict_1[item] -= 1
            result.append(item)

    return result

